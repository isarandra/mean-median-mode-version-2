package CalculationTest;

import Calculation.ICalculation;
import Calculation.Mode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ModeTest {
    ICalculation test;

    @BeforeEach
    void start(){
        test = new Mode();
    }

    @Test
    void mode_oneMode(){
        int[] input = {6,3,9,6,6,5,9,3};
        List<Double> expected = new ArrayList<>(Arrays.asList(6.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void mode_oneInput(){
        int[] input = {23};
        List<Double> expected = new ArrayList<>(Arrays.asList(23.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void mode_twoMode(){
        int[] input = {1,3,3,3,5,6,6,9,9,9};
        List<Double> expected = new ArrayList<>(Arrays.asList(3.0,9.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void mode_manyInput(){
        int[] input = {4,10,27,12,4,4,12,27,7,34,12,48,3,34,12,42,4};
        List<Double> expected = new ArrayList<>(Arrays.asList(4.0,12.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void mode_allNumberAppearranceIsOne(){
        int[] input = {7,3,8,11,5,19,4,10};
        List<Double> expected = new ArrayList<>();
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void mode_inputZero(){
        int[] input = {0};
        List<Double> expected = new ArrayList<>(Arrays.asList(0.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void mode_inputNull(){
        int[] input = null;
        List<Double> expected = new ArrayList<>();
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }
}

