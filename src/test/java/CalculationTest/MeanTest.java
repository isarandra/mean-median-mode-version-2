package CalculationTest;

import Calculation.ICalculation;
import Calculation.Mean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MeanTest {

    ICalculation test;

    @BeforeEach
    void start(){
        test = new Mean();
    }

    @Test
    void mean_inputFiveNumbers(){
        int[] input = {1,2,3,4,5};
        List<Double> expected = new ArrayList<>(Arrays.asList(3.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void mean_inputSixNumbers(){
        int[] input = {2,2,5,6,7,8};
        List<Double> expected = new ArrayList<>(Arrays.asList(5.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void mean_inputEightNumbers(){
        int[] input = {6,7,10,12,13,4,8,12};
        List<Double> expected = new ArrayList<>(Arrays.asList(72/8.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void mean_inputThirtyNumbers(){
        int[] input = {10,20,36,92,95,40,50,56,60,70,92,88,80,70,72,70,36,40,36,40,92,40,50,50,56,60,70,60,60,88};
        List<Double> expected = new ArrayList<>(Arrays.asList(1779/30.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void mean_zeroInput(){
        int[] input = {0};
        List<Double> expected = new ArrayList<>(Arrays.asList(0.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void mean_nullInput(){
        List<Double> expected = new ArrayList<>();
        int[] input = null;
        List<Double> result=test.calculation(input);
        Assertions.assertEquals(expected,result);
    }
}

