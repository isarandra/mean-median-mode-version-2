package CalculationTest;

import Calculation.ICalculation;
import Calculation.Median;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MedianTest {
    ICalculation test;

    @BeforeEach
    void start(){
        test = new Median();
    }

    @Test
    void median_inputOddNumbersSorted(){
        int[] input = {1,3,3,6,7,8,9};
        List<Double> expected = new ArrayList<>(Arrays.asList(6.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void median_inputEvenNumbersSorted(){
        int[] input = {1,2,3,4,5,6,8,9};
        List<Double> expected = new ArrayList<>(Arrays.asList(4.5));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void median_oddInputNumbersNotSorted(){
        int[] input = {8,14,8,45,1,31,16,40,12,30,42,30,24};
        List<Double> expected = new ArrayList<>(Arrays.asList(24.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void median_evenInputNumbersNotSorted(){
        int[] input = {3,50,43,18,14,8,38,47};
        List<Double> expected = new ArrayList<>(Arrays.asList(28.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void median_zeroInput(){
        int[] input = {0};
        List<Double> expected = new ArrayList<>(Arrays.asList(0.0));
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void median_nullInput(){
        int[] input = null;
        List<Double> expected = new ArrayList<>();
        List<Double> result = test.calculation(input);
        Assertions.assertEquals(expected,result);
    }
}

