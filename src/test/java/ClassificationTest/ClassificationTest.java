package ClassificationTest;

import Classification.Classification;
import Classification.IClassification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.TreeMap;

public class ClassificationTest {
    IClassification test;

    @BeforeEach
    void start(){
        test=new Classification();
    }

    @Test
    void calculation_allOneAppearanceSorted(){
        int[] input = {5,8,13,15,17};
        TreeMap<Integer,Integer> result = new TreeMap<>();
        result.putAll(test.calculation(input));

        TreeMap<Integer,Integer> expected = new TreeMap<>();
        expected.put(5,1);
        expected.put(8,1);
        expected.put(13,1);
        expected.put(15,1);
        expected.put(17,1);

        Assertions.assertTrue(result.equals(expected));
    }

    @Test
    void calculation_twoModeSorted(){
        int[] input = {1,3,3,3,5,6,6,9,9,9};
        TreeMap<Integer,Integer> result = new TreeMap<>();
        result.putAll(test.calculation(input));

        TreeMap<Integer,Integer> expected = new TreeMap<>();
        expected.put(1,1);
        expected.put(3,3);
        expected.put(5,1);
        expected.put(6,2);
        expected.put(9,3);

        Assertions.assertTrue(result.equals(expected));
    }

    @Test
    void calculation_oneModeNotSorted(){
        int[] input = {6,3,9,6,6,5,9,3};
        TreeMap<Integer,Integer> result = new TreeMap<>();
        result.putAll(test.calculation(input));

        TreeMap<Integer,Integer> expected = new TreeMap<>();
        expected.put(6,3);
        expected.put(3,2);
        expected.put(9,2);
        expected.put(5,1);

        Assertions.assertTrue(result.equals(expected));
    }

    @Test
    void calculation_oneModeNotSorted2(){
        int[] input = {10,20,15,20,25,30,35,20,20,30,15};
        TreeMap<Integer,Integer> result = new TreeMap<>();
        result.putAll(test.calculation(input));

        TreeMap<Integer,Integer> expected = new TreeMap<>();
        expected.put(10,1);
        expected.put(15,2);
        expected.put(20,4);
        expected.put(25,1);
        expected.put(30,2);
        expected.put(35,1);

        Assertions.assertTrue(result.equals(expected));
    }

    @Test
    void calculation_inputNull(){
        int[] input = null;
        TreeMap<Integer,Integer> result = new TreeMap<>();
        result.putAll(test.calculation(input));

        TreeMap<Integer,Integer> expected = new TreeMap<>();

        Assertions.assertTrue(result.equals(expected));
    }
}
