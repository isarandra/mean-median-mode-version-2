package FileHandlerTest;

import FileHandler.CSVFileHandler;
import FileHandler.IFileHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CSVFileHandlerTest {

    IFileHandler test;

    @BeforeEach
    void start(){
        test=new CSVFileHandler();
    }

    @Test
    void readFile_inputNull(){
        String input=null;
        int[] expected = null;
        int[] result = test.readFile(input);
        Assertions.assertArrayEquals(expected,result);
    }

    @Test
    void readFile_fileNotFound(){
        String input="lorem ipsum";
        int[] expected = null;
        int[] result = test.readFile(input);
        Assertions.assertArrayEquals(expected,result);
    }

    @Test
    void readFile_fileIsFound(){
        String input="C:\\Users\\USER\\Desktop\\SYNRGY Academy\\new_Challenge_3\\src\\main\\resources\\test.csv";
        int[] expected = {1,3,77,24,36,77,54,23,65};
        int[] result = test.readFile(input);
        Assertions.assertArrayEquals(expected,result);
    }
}
