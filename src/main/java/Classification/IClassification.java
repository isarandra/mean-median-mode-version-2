package Classification;

import java.util.Map;

public interface IClassification {

    Map<Integer,Integer> calculation(int[] input);
}
