package Classification;

import java.util.List;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Classification implements IClassification{
    @Override
    public Map<Integer,Integer> calculation(int[] input) throws NullPointerException{

        Integer countLessThanSix=0;
        Map<Integer,Integer> numberAppearance = new HashMap<>();

        try {
            List<Integer> inputAsList = Arrays.stream(input).boxed().collect(Collectors.toList());
            countLessThanSix=Integer.valueOf((int)inputAsList.stream().filter(number -> number<6).count());
            BufferedWriter writer = new BufferedWriter(new FileWriter("classification.txt"));
            writer.write("Score  | Frequency\n");
            writer.write("< 6");
            writer.write("    |     ");
            writer.write(countLessThanSix.toString());
            for(int number:input){
                if(numberAppearance.containsKey(number)){
                    numberAppearance.computeIfPresent(number,(key, val)->val+1);
                }
                else{
                    numberAppearance.computeIfAbsent(number,k->1);
                }
            }
            Map<Integer, Integer> filteringLessThanSeven = new LinkedHashMap<>();
            numberAppearance.entrySet().stream().filter(number -> number.getKey() >= 6)
                    .forEach(number -> filteringLessThanSeven.put(number.getKey(),number.getValue()));
            filteringLessThanSeven.entrySet().stream()
                    .forEach(number -> {
                        try {
                            writer.write("\n");
                            writer.write(number.getKey().toString());
                            writer.write("      |     ");
                            writer.write(number.getValue().toString());
                        }
                        catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
            writer.close();
            return numberAppearance;
        }
        catch (NullPointerException nullPointer){
            System.out.println("input for classification method cannot be null");
            return numberAppearance;
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
