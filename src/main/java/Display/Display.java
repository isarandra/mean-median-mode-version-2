package Display;

import Calculation.ICalculation;
import Calculation.Mean;
import Calculation.Median;
import Calculation.Mode;
import Classification.Classification;
import Classification.IClassification;
import FileHandler.CSVFileHandler;
import FileHandler.IFileHandler;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Display {

    static Scanner scanner = new Scanner(System.in);

    public static void menu() {

        IFileHandler file = new CSVFileHandler();

        System.out.println("\nType name and location of the file containing the data\n" +
                "(example: C:\\Users\\USER\\Desktop\\JAVA PROJECTS - ISA RANDRA" +
                "\\Mean Median Mode Version 2\\src\\main\\resources\\school_data.csv)");
        try {
            String fileNameAndLocation = scanner.nextLine();

            int[] input = file.readFile(fileNameAndLocation);

            if (input == null) {
                thirdSlide();
            }

            System.out.println();
            System.out.println("Choose menu:\n" +
                    "1. Generate mode.txt\n" +
                    "2. Generate mean-median.txt\n" +
                    "3. Generate mode.txt and mean-median.txt\n" +
                    "4. Generate classification.txt\n" +
                    "0. Exit");

            int mainMenuControl = scanner.nextInt();

            switch (mainMenuControl) {
                case 1:
                    ICalculation mode1 = new Mode();
                    mode1.calculation(input);
                    secondMenu();
                    break;
                case 2:
                    ICalculation mean1 = new Mean();
                    ICalculation median1 = new Median();
                    mean1.calculation(input);
                    median1.calculation(input);
                    secondMenu();
                    break;
                case 3:
                    ICalculation mode2 = new Mode();
                    ICalculation mean2 = new Mean();
                    ICalculation median2 = new Median();
                    mode2.calculation(input);
                    mean2.calculation(input);
                    median2.calculation(input);
                    secondMenu();
                    break;
                case 4:
                    IClassification classification = new Classification();
                    classification.calculation(input);
                    secondMenu();
                    break;
                case 0:
                    System.exit(0);
                default:
                    throw new InputMismatchException();
            }
            scanner.close();
        }
        catch (InputMismatchException inputMismatchException){
            System.out.println("\nPlease insert the right value and try again");
            System.exit(1);
        }
        catch (Exception exception){

        }
    }

    private static void thirdSlide() {
        try{
            System.out.println("\nChoose menu:\n" +
                    "1. Back to main menu\n" +
                    "0. Exit");

            int thirdSlideControl = scanner.nextInt();
            System.out.println("------------------------------------------------------------------");
            switch (thirdSlideControl){
                case 1:
                    scanner.nextLine();
                    menu();
                    break;
                case 0:
                    scanner.close();
                    System.exit(0);
                default:
                    throw new InputMismatchException();
            }
        }
        catch (InputMismatchException inputMismatchException){
            System.out.println("\nPlease insert the right value and try again");
            System.exit(0);
        }
    }

    private static void secondMenu() {
        try{
            System.out.println("------------------------------------------------------------------");
            System.out.println("File has been generated at Mean Median Mode Version 2 folder\n");
            System.out.println("Choose menu: \n" +
                    "1. Back to main menu\n" +
                    "0. Exit");
            int secondMenuControl = scanner.nextInt();
            System.out.println("------------------------------------------------------------------");
            switch (secondMenuControl){
                case 0:
                    scanner.close();
                    System.exit(0);
                    break;
                case 1:
                    scanner.nextLine();
                    menu();
                default:
                    throw new InputMismatchException();
            }
            scanner.close();
        }
        catch (InputMismatchException inputMismatchException){
            System.out.println("\nPlease insert the right value and try again");
            System.exit(0);
        }
    }

    public static void header(String string) {
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println(string);
        System.out.println("---------------------------------------------------------------------------------------------------");
    }
}

