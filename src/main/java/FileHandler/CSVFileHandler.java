package FileHandler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class CSVFileHandler implements IFileHandler {

    private int[] input;

    @Override
    public int[] readFile(String file) throws NullPointerException{
        BufferedReader reader = null;
        String line = "";
        List<Integer> inputArray = new ArrayList<>();
        try{
            reader = new BufferedReader(new FileReader(file));
            while((line = reader.readLine()) != null){
                String[] row = line.split(";");
                IntStream.range(1, row.length).forEach(i -> inputArray.add(Integer.parseInt(row[i])));
            }
            input = inputArray.stream().mapToInt(i->i).toArray();
        }
        catch (IOException ioException){
            System.out.println("\nFile not found, make sure file name and location is valid");
            return input;
        }
        catch (NullPointerException nullPointerException){
            System.out.println("input for readFile method cannot be null");
            return input;
        }
        finally {
            try{
                reader.close();
            }
            catch (Exception e){
            }
        }
        return input;
    }
}
