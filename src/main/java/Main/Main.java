package Main;

import Display.Display;

public class Main {

    public static void main(String[] args) {

        Display display = new Display();
        display.header("Student Statistics App");
        display.menu();
    }
}
