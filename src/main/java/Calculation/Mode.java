package Calculation;

import java.util.List;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Mode implements ICalculation{

    @Override
    public List<Double> calculation(int[] input) throws NullPointerException{
        List<Double> output = new ArrayList<>();
        try {
            Map<Integer,Integer> numberAppearance = new HashMap<>();

            for(int number:input){
                if(numberAppearance.containsKey(number)){
                    numberAppearance.computeIfPresent(number,(key, val)->val+1);
                }
                else{
                    numberAppearance.computeIfAbsent(number,k->1);
                }
            }

            int modeValue = -1;
            Integer modeKey=0;
            int numberOfMode=0;

            List<Integer> modeKeyList=new ArrayList<>();

            for(Map.Entry<Integer, Integer> number:numberAppearance.entrySet()){
                if(number.getValue()>modeValue){
                    modeValue=number.getValue();
                    modeKey=number.getKey();
                }
            }
            for(Map.Entry<Integer, Integer> number:numberAppearance.entrySet()){
                if(number.getValue()==modeValue){
                    modeKeyList.add(number.getKey());
                    numberOfMode++;
                }
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter("mode.txt"));
            if(input.length==1){
                writer.write("Mode: ");
                writer.write(modeKey.toString());
                output.add((double)modeKey);
                writer.close();
            }
            else if(numberOfMode == numberAppearance.size()){
                writer.write("Mode: ");
                writer.write(output.toString());
                writer.close();
            }
            else if(numberOfMode>1){
                //for output txt
                int[] modeKeyListInt = modeKeyList.stream()
                        .mapToInt(i->i)
                        .toArray();
                String[] modeKeyListString = Arrays.stream(modeKeyListInt)
                        .mapToObj(String::valueOf)
                        .toArray(String[]::new);
                writer.write("Mode: ");
                for(int i=0;i<modeKeyListString.length;i++){
                    writer.write(modeKeyListString[i]);
                    output.add(Double.valueOf(modeKeyListString[i]));
                    if(i!=modeKeyListString.length-1){
                        writer.write(", ");
                    }
                }
                writer.close();
            }
            else {
                writer.write("Mode: ");
                writer.write(modeKey.toString());
                output.add((double)modeKey);
                writer.close();
            }
            return output;
        }
        catch (NullPointerException nullPointer){
            System.out.println("input for mode method cannot be null");
            return output;
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
