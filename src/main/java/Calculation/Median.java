package Calculation;

import java.util.ArrayList;
import java.util.List;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Median implements ICalculation{

    private int count = 0; //for recursive at sort method;

    @Override
    public List<Double> calculation(int[] input) throws NullPointerException{
        double medianIndex;
        Double median;
        List<Double> output = new ArrayList<>();

        try {
            sort(input);
            BufferedWriter writer = new BufferedWriter(new FileWriter("mean-median.txt", true));
            if(input.length%2==0){
                median=(input[input.length/2-1]+input[input.length/2])/2.0;
                writer.write("\nMedian: ");
                writer.write(median.toString());
                output.add(median);
                writer.close();
            }
            else{
                medianIndex=input.length/2.0;
                median=(double)input[(int)medianIndex];
                writer.write("\nMedian: ");
                writer.write(median.toString());
                output.add(median);
                writer.close();
            }
            return output;
        }
        catch (NullPointerException nullPointer){
            System.out.println("input for median method cannot be null");
            return output;
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void sort(int[] list) {
        int dummy = 0;
        if (count == list.length){
            return;
        }
        for(int i=1;i<list.length;i++){
            if(list[i-1]>list[i]){
                dummy=list[i-1];
                list[i-1]=list[i];
                list[i]=dummy;
            }
        }
        count++;
        sort(list);
    }
}
