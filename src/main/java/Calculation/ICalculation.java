package Calculation;

import java.util.List;

public interface ICalculation {
    List<Double> calculation(int[] input);
}
