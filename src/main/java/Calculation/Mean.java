package Calculation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Mean implements ICalculation{

    @Override
    public List<Double> calculation(int[] input) throws NullPointerException{

        Double mean;
        List<Double> output = new ArrayList<>();

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("mean-median.txt"));
            List<Integer> inputList = Arrays.stream(input).boxed().collect(Collectors.toList());
            mean=inputList.stream().collect(Collectors.summarizingInt(Integer::valueOf)).getAverage();
            output.add(mean);
            writer.write("Mean: ");
            writer.write(mean.toString());
            writer.close();
            return output;
        }
        catch (NullPointerException nullPointer){
            System.out.println("input for mean method cannot be null");
            return output;
        }
        catch (IOException e) {
            System.out.println("Error in "+this.getClass()+" method "
                    +new Exception().getStackTrace()[0].getMethodName());
            return output;
        }
    }
}
